﻿using System;
using System.Collections.Generic;
using DropingDrops;
using System.Json;
using CoreLocation;
using CoreMotion;
using Foundation;
using UIKit;
using OpenTK;
using System.Threading;
using System.Threading.Tasks;
using System.Diagnostics;
using DropingDrops.iOS;


[assembly: Xamarin.Forms.Dependency(typeof(DropGeo))]
namespace DropingDrops.iOS
{
	public class DropGeo : DropGeoCoordinates,IDeviceMotion,IDisposable
	{
		
		protected double ms = 1000.0;
		public string WorldOrUrl { get; private set; }
		protected IDictionary<MotionSensorType, bool> sensorStatus;
		protected CLLocationManager locationManager;
		protected CLLocationManager locationM;
		protected CLLocation location;
		public CMMotionManager motionManager;
		protected CMMotionManager mManager;
		protected double Azimuth;
		protected double Inclination;
		protected JsonArray poiData;
		protected NSError error;
		protected List<float[]> mRotHist;
		protected int mRotHistIndex;
		protected int mHistoryMaxLength = 40;
		protected float[] rMat;
		public bool IsDataValid { get; protected set; }
		protected double heading;
		const double EarthRadius = 6371000;
		List<double> l;

		public DropGeo()
		{
			
			this.location = new CLLocation();
			motionManager = new CMMotionManager();
			locationManager = new CLLocationManager();
			mManager = new CMMotionManager();
			locationM = new CLLocationManager();
			this.poiData = new JsonArray();
			this.Azimuth = new double();
			this.Inclination = new double();
			this.l= new List<double>();
			mRotHist = new List<float[]>();
			locationManager.DesiredAccuracy = CLLocation.AccuracyBest;
			locationManager.HeadingFilter = 1;
			sensorStatus = new Dictionary<MotionSensorType, bool>(){
				{ MotionSensorType.Accelerometer, false},
				{ MotionSensorType.Gyroscope, false},
				{ MotionSensorType.Magnetometer, false},
				{ MotionSensorType.Compass, false}
			};
			this.heading = new double();
		}


		static double[] POICoordinates(double lat, double lon, double alti, double az, double incli, double distance)
		{
			az = az * Math.PI / 180;

			var b = distance / EarthRadius;
			var a = Math.Acos(Math.Cos(b) * Math.Cos((90 - lat) * Math.PI / 180) + Math.Sin((90 - lat) * Math.PI / 180) * Math.Sin(b) * Math.Cos(az));
			var c = Math.Asin(Math.Sin(b) * Math.Sin(az) / Math.Sin(a));

			var newLat = 90 - (a * 180 / Math.PI);
			var newLng = (c * 180 / Math.PI) + lon;
			var newAlt = alti + Math.Tan(incli) * distance;

			return new double[] { newLat, newLng, newAlt };
		}
		public void afficher(List<double> h)
		{
			for (int i = 0; i < h.Count; i++)
			{
				Console.WriteLine("Drop ppppppp£" + h[i]);

			}

			}
		async  public  void getdropgeocoordinates(double distance)
		{
			this.locationManager.StartUpdatingLocation();
			location = locationManager.Location;
			this.locationManager.StopUpdatingLocation();
		 Start(MotionSensorType.Compass, MotionSensorDelay.Default);


			var loc = new double[3];
			motionManager.DeviceMotionUpdateInterval = 0.1;

			motionManager.StartDeviceMotionUpdates(motionManager.AttitudeReferenceFrame);


			mManager = motionManager;
			await Task.Delay(1000);
			CMAcceleration gravity = mManager.DeviceMotion.Gravity;

			Vector2 vect2 = new Vector2((float)-gravity.X, (float)-gravity.Y);
			double tiltAngle = angleYAxisToVector(vect2);
			double newHeading = (this.heading + tiltAngle) % 360;
			float r = (float)Math.Sqrt(gravity.X * gravity.X + gravity.Y * gravity.Y + gravity.Z * gravity.Z);
			this.Inclination = -(Math.Acos(gravity.Z / r) - Math.PI / 2);

			Console.WriteLine("dadas", +this.location.Coordinate.Latitude, +this.location.Coordinate.Longitude, +this.location.Coordinate.Latitude, +this.location.Coordinate.Latitude, +this.location.Coordinate.Latitude, +this.location.Coordinate.Latitude, +this.location.Altitude, +newHeading, +this.Inclination, +distance);

			loc = POICoordinates(this.location.Coordinate.Latitude, this.location.Coordinate.Longitude, this.location.Altitude, newHeading, this.Inclination, distance);
			Console.WriteLine("0000£" + tiltAngle);
			Console.WriteLine("1111£" + Inclination);
			Console.WriteLine("2222£" + newHeading);
			Console.WriteLine("3333£" + this.heading);

			//Stop(MotionSensorType.Compass);
			Start(MotionSensorType.Compass, MotionSensorDelay.Default);
			//Stop(MotionSensorType.Compass);
			//StopUpdatesWithMotionType(mManager);
			//StopUpdatesWithMotionType(motionManager);
			Console.WriteLine("Drop Latitude£" + loc[0]);
			Console.WriteLine("Drop Longitude£" + loc[1]);
			Console.WriteLine("Drop Altitude£" + loc[2]);
			for (int i = 0; i < 3; i++)
			{
				l.Add(loc[i]);
			}
			for (int i = 0; i < l.Count; i++)
			{
				Console.WriteLine("Drop m£" + l[i]);

			}



		}
		public List<double> getpoi()
		{ 
			return l;
			}
		public void  Start()
		{
		Start(MotionSensorType.Compass, MotionSensorDelay.Default);
			Stop(MotionSensorType.Compass);
			}
		public void StopUpdatesWithMotionType(CMMotionManager d)
		{
			d.StopDeviceMotionUpdates();
		}

		public static double angleYAxisToVector(Vector2 v)
		{
			double dX = v.X;
			double dY = v.Y;

			if (dY == 0)
			{
				if (dX > 0)
				{
					return 0.0;
				}
				else {
					if (dX < 0)
					{
						return 180.0;
					}
					else {
						return -1;
					}
				}
			}

			double beta = Math.Atan(dX / dY) * 180.0 / Math.PI;
			double angle;
			if (dX > 0)
			{
				if (dY < 0)
				{
					angle = 180 + beta;
				}
				else {
					angle = beta;
				}
			}
			else {
				if (dY < 0)
				{
					angle = 180 + beta;
				}
				else {
					angle = 360 + beta;
				}
			}
			//    NSLog(@"angle = %f, normalized = %f",beta,angle);
			return angle;
		}
	
	
	public event SensorValueChangedEventHandler SensorValueChanged;

		/// <summary>
		/// Start the specified sensorType and interval.
		/// </summary>
		/// <param name="sensorType">Sensor type.</param>
		/// <param name="interval">Interval.</param>
		public void Start(MotionSensorType sensorType, MotionSensorDelay interval)
		{

			switch (sensorType)
			{
				case MotionSensorType.Accelerometer:
					if (motionManager.AccelerometerAvailable)
					{
						motionManager.AccelerometerUpdateInterval = (double)interval / ms;
						motionManager.StartAccelerometerUpdates(NSOperationQueue.CurrentQueue, OnAccelerometerChanged);
					}
					else
					{
						Debug.WriteLine("Accelerometer not available");
					}
					break;
				case MotionSensorType.Gyroscope:
					if (motionManager.GyroAvailable)
					{
						motionManager.GyroUpdateInterval = (double)interval / ms;
						motionManager.StartGyroUpdates(NSOperationQueue.CurrentQueue, OnGyroscopeChanged);
					}
					else
					{
						Debug.WriteLine("Gyroscope not available");
					}
					break;
				case MotionSensorType.Magnetometer:
					if (motionManager.MagnetometerAvailable)
					{
						motionManager.MagnetometerUpdateInterval = (double)interval / ms;
						motionManager.StartMagnetometerUpdates(NSOperationQueue.CurrentQueue, OnMagnometerChanged);
					}
					else
					{
						Debug.WriteLine("Magnetometer not available");
					}
					break;
				case MotionSensorType.Compass:
					if (CLLocationManager.HeadingAvailable)
					{
						locationManager.StartUpdatingHeading();
						locationManager.UpdatedHeading += OnHeadingChanged;
					}
					else
					{
						Debug.WriteLine("Compass not available");
					}
					break;
			}
			sensorStatus[sensorType] = true;
		}

		public void OnHeadingChanged(object sender, CLHeadingUpdatedEventArgs e)
		{
			if (SensorValueChanged == null)
			{
				this.heading = e.NewHeading.TrueHeading;
				return;
			}
			SensorValueChanged(this, new SensorValueChangedEventArgs { ValueType = MotionSensorValueType.Single, SensorType = MotionSensorType.Compass, Value = new MotionValue { Value = e.NewHeading.TrueHeading } });
			this.heading = e.NewHeading.TrueHeading;
		}

		/// <summary>
		/// Raises the magnometer changed event.
		/// </summary>
		/// <param name="data">Data.</param>
		/// <param name="error">Error.</param>
		public void OnMagnometerChanged(CMMagnetometerData data, NSError error)
		{
			if (SensorValueChanged == null)
				return;

			SensorValueChanged(this, new SensorValueChangedEventArgs { ValueType = MotionSensorValueType.Vector, SensorType = MotionSensorType.Magnetometer, Value = new MotionVector() { X = data.MagneticField.X, Y = data.MagneticField.Y, Z = data.MagneticField.Z } });

		}

		/// <summary>
		/// Raises the accelerometer changed event.
		/// </summary>
		/// <param name="data">Data.</param>
		/// <param name="error">Error.</param>
		public void OnAccelerometerChanged(CMAccelerometerData data, NSError error)
		{
			if (SensorValueChanged == null)
				return;

			SensorValueChanged(this, new SensorValueChangedEventArgs { ValueType = MotionSensorValueType.Vector, SensorType = MotionSensorType.Accelerometer, Value = new MotionVector() { X = data.Acceleration.X, Y = data.Acceleration.Y, Z = data.Acceleration.Z } });

		}

		/// <summary>
		/// Raises the gyroscope changed event.
		/// </summary>
		/// <param name="data">Data.</param>
		/// <param name="error">Error.</param>
		public void OnGyroscopeChanged(CMGyroData data, NSError error)
		{
			if (SensorValueChanged == null)
				return;

			SensorValueChanged(this, new SensorValueChangedEventArgs { ValueType = MotionSensorValueType.Vector, SensorType = MotionSensorType.Gyroscope, Value = new MotionVector() { X = data.RotationRate.x, Y = data.RotationRate.y, Z = data.RotationRate.z } });

		}

		public void Stop(MotionSensorType sensorType)
		{
			switch (sensorType)
			{
				case MotionSensorType.Accelerometer:
					if (motionManager.AccelerometerActive)
						motionManager.StopAccelerometerUpdates();
					else
						Debug.WriteLine("Accelerometer not available");
					break;
				case MotionSensorType.Gyroscope:
					if (motionManager.GyroActive)
						motionManager.StopGyroUpdates();
					else
						Debug.WriteLine("Gyroscope not available");
					break;
				case MotionSensorType.Magnetometer:
					if (motionManager.MagnetometerActive)
						motionManager.StopMagnetometerUpdates();
					else
						Debug.WriteLine("Magnetometer not available");
					break;
				case MotionSensorType.Compass:
					if (CLLocationManager.HeadingAvailable)
					{
						locationManager.StopUpdatingHeading();
						locationManager.UpdatedHeading -= OnHeadingChanged;
					}
					else
					{
						Debug.WriteLine("Compass not available");
					}
					break;
			}
			sensorStatus[sensorType] = false;
		}

		/// <summary>
		/// Determines whether this instance is active the specified sensorType.
		/// </summary>
		/// <returns><c>true</c> if this instance is active the specified sensorType; otherwise, <c>false</c>.</returns>
		/// <param name="sensorType">Sensor type.</param>
		public bool IsActive(MotionSensorType sensorType)
		{
			return sensorStatus[sensorType];
		}
		#region IDisposable implementation
		/// <summary>
		/// Dispose of class and parent classes
		/// </summary>
		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		/// <summary>
		/// Dispose up
		/// </summary>
		~DropGeo()
		{
			Dispose(false);
		}
		private bool disposed = false;
		/// <summary>
		/// Dispose method
		/// </summary>
		/// <param name="disposing"></param>
		public virtual void Dispose(bool disposing)
		{
			if (!disposed)
			{
				if (disposing)
				{
					//dispose only
				}

				disposed = true;
			}
		}


		#endregion

	}

}
