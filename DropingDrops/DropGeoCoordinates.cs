﻿using System;
using System.Collections.Generic;
using DropingDrops;
namespace DropingDrops
{
	public interface DropGeoCoordinates
	{
		void getdropgeocoordinates(double distance);
		List<double> getpoi();
		void afficher(List<double> h);
	}
}
